Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :geo do
      resource :coordinate do
        get :address
        get :ip
      end
      resource :distance, only: :show
      resource :about do
        get :country
      end
    end
  end
end
