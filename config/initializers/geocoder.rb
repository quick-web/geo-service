Geocoder.configure(

    # geocoding service (see below for supported options):
    :lookup => :google,

    # IP address geocoding service (see below for supported options):
    :ip_lookup => :ipinfo_io,

    # to use an API key:
    :api_key => ENV['GOOGLE_API_KEY'] || '',

    # geocoding service request timeout, in seconds (default 3):
    :timeout => 5,

    # set default units to kilometers:
    :units => :mi,

    # caching (see below for details):
    :cache => Redis.new(host: ENV['REDIS_HOST'] || '127.0.0.1', port: 6379),
    #:cache_prefix => "geo"

)