class Api::Geo::CoordinatesController < ApplicationController

  def address
    render json: result {
       coords = Geocoder.coordinates(params[:query])
       raise 'Invalid address' unless coords.present?
       {lat: coords.first, lng: coords.last}
    }
  rescue => e
    render json: result(e.message)
  end

  def ip

    ipaddr = IPAddr.new params[:query]
    if ipaddr.ipv6?
      geocoder_result = Geocoder.search(params[:query]).first
      geocoder_result.blank? && raise # no result, tryining geocoder
      country = ISO3166::Country.new(geocoder_result.country).translated_names.first rescue nil
      data = {
          countryCode: geocoder_result.country,
          country: country,
          region: geocoder_result.region.blank? ? nil : geocoder_result.region,
          city: geocoder_result.city.blank? ? nil : geocoder_result.city,
          postal: geocoder_result.postal,
          lat: geocoder_result.coordinates.first.to_f,
          lng: geocoder_result.coordinates.last.to_f,
      }
      render json: result { data }
    elsif ipaddr.ipv4?
      i2l = Ip2location.new.open(Rails.root.join('db','iplocaton.bin'))
      record = i2l.get_all(params[:query])
      country = ISO3166::Country.new(record.country_short.to_s).translated_names.first rescue nil
      data = {
          countryCode: record.country_short.to_s,
          country: country,
          region: record.region.to_s,
          city: record.city.to_s,
          postal: nil,
          lat: record.latitude.to_f,
          lng: record.longitude.to_f
      }
      render json: result { data }
    else
      raise 'Invalid address'
    end

  rescue => e
    Rails.logger.warn 'No geocoder result: ' << e.message << '. params: ' << params.to_json
    render json: result(e.message) {
      {
          countryCode: nil,
          country: nil,
          region: nil,
          city: nil,
          lat: nil,
          lng: nil
      }
    }
  end

  private
    def result(error = nil)
      coordinates = yield if block_given?
      coordinates ||= {lat: nil, lng: nil}
      {success: error.nil?, error: error, location: coordinates}
    end
end