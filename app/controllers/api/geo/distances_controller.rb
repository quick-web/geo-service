class Api::Geo::DistancesController < ApplicationController
  def show
      from = [params[:from][:lat], params[:from][:lng]].map(&:to_f) rescue []
      to =  [params[:to][:lat], params[:to][:lng]].map(&:to_f) rescue []
      unit = params[:units].to_sym rescue :mi
      distance = Geocoder::Calculations.distance_between(from, to, {units: unit})
      render json: {
          success: ! distance.nan?, error: ('invalid params' if distance.nan?), result: {distance: distance, units: unit}
      }
  rescue => e
    render json: {
        success: false, error: e.message, result: {distance: nil, units: nil}
    }
  end

end