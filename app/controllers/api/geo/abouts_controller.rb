class Api::Geo::AboutsController < ApplicationController
  def country
    country_code = params[:query]
    country = ISO3166::Country.new(country_code)
    render json: {
        success: country.present?, error: country.present? ? nil : 'Not found', result: country
    }
  rescue => e
    render json: {
        success: false, error: e.message, result: nil
    }
  end
end