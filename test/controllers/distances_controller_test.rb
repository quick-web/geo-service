require 'test_helper'

class DistancesControllerTest < ActionDispatch::IntegrationTest

  # distance
  def test_failed
    get api_geo_distance_url
    assert_equal false, json_response['success']

    get api_geo_distance_url, params: {from: {lat: 47.858205, lng: 2.294359}, to: {lat: 40.748433, lng: -73.985655}, units: 'invalid'}
    assert_equal false, json_response['success']

    assert_response :success
  end

  def test_success
    get api_geo_distance_url, params: {from: {lat: 47.858205, lng: 2.294359}, to: {lat: 40.748433, lng: -73.985655}}
    assert_equal true, json_response['success']
    assert_equal 3648.3340765758867, json_response['result']['distance']
    assert_equal 'mi', json_response['result']['units']

    get api_geo_distance_url, params: {from: {lat: 47.858205, lng: 2.294359}, to: {lat: 40.748433, lng: -73.985655}, units: 'km'}
    assert_equal true, json_response['success']
    assert_equal 5871.424558375546, json_response['result']['distance']
    assert_equal 'km', json_response['result']['units']

    get api_geo_distance_url, params: {from: {lat: 47.858205, lng: 2.294359}, to: {lat: 40.748433, lng: -73.985655}, units: 'nm'}
    assert_equal true, json_response['success']
    assert_equal 3170.316790266785, json_response['result']['distance']
    assert_equal 'nm', json_response['result']['units']

    assert_response :success

  end

end
