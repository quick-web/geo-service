require 'test_helper'

class CoordinatesControllerTest < ActionDispatch::IntegrationTest

  # Address coordinates
  def test_address_failed
    get address_api_geo_coordinate_url
    assert_equal false, json_response['success']
    assert_equal 'Invalid address', json_response['error']

    get address_api_geo_coordinate_url, params: {query: 'hello world'}
    assert_equal false, json_response['success']
    assert_equal 'Invalid address', json_response['error']

    assert_response :success
  end

  def test_address_success
    get address_api_geo_coordinate_url, params: {query: 'New York, NY'}
    assert_equal true, json_response['success']
    assert_equal 40.7127837, json_response['location']['lat']
    assert_equal -74.0059413, json_response['location']['lng']

    get address_api_geo_coordinate_url, params: {query: 'Russia, Moscow'}
    assert_equal true, json_response['success']
    assert_equal 55.755826, json_response['location']['lat']
    assert_equal 37.6173, json_response['location']['lng']
    assert_response :success
  end

  # IP coordinates
  def test_ip_failed
    get ip_api_geo_coordinate_url
    assert_equal false, json_response['success']

    get ip_api_geo_coordinate_url, params: {query: '-1'}
    assert_equal false, json_response['success']

    assert_response :success
  end

  def test_ip_success
    get ip_api_geo_coordinate_url, params: {query: '8.8.8.8'}
    assert_equal true, json_response['success']
    assert_equal 'US', json_response['location']['countryCode']
    assert_equal 'United States', json_response['location']['country']
    assert_equal 'California', json_response['location']['region']
    assert_equal 'Mountain View', json_response['location']['city']
    assert_equal true, json_response['location']['lat'].present?
    assert_equal true, json_response['location']['lng'].present?

    get ip_api_geo_coordinate_url, params: {query: '74.95.165.158'}
    assert_equal true, json_response['success']
    assert_equal 'US', json_response['location']['countryCode']
    assert_equal 'United States', json_response['location']['country']

    assert_equal 'Pennsylvania', json_response['location']['region']
    assert_equal 'Philadelphia', json_response['location']['city']
    assert_equal true, json_response['location']['lat'].present?
    assert_equal true, json_response['location']['lng'].present?

    assert_response :success
  end

end
