# Geo Service

Framework: Ruby On Rails

### Enviroment
```REDIS_HOST``` (default: 127.0.0.1)

```GOOGLE_API_KEY``` (default: empty)

```SECRET_KEY_BASE``` (rails secret)

## Start Service
```> docker-compose up```

## Resources

### Address Location
> Get address coordinate

* Action: `/api/geo/coordinate/address`
* Method: GET
* Required Params: `{query: "Russia,Moscow"}`
* Success Response:
```
{
   "success": true,
   "error": null,
   "location": {
     "lat": 55.755826,
     "lng": 37.6173
   }
 }
```
* Error Response:
```
{
  "success": false,
  "error": "Invalid address",
  "location": {
    "lat": null,
    "lng": null
  }
}
```
### IP address location
* Action: `/api/geo/coordinate/ip`
* Method: GET
* Required Params: `{query: "8.8.8.8"}`
* Success Response:
```
{
  "success": true,
  "error": null,
  "location": {
    "countryCode": "US",
    "country": "United States",
    "region": "California",
    "city": "Mountain View",
    "postal": "94035",
    "lat": 37.40599060058594,
    "lng": -122.0785140991211
  }
}
```
* Error Response:
```
{
  "success": false,
  "error": "invalid address",
  "location": {
    "countryCode": null,
    "country": null,
    "region": null,
    "city": null,
    "lat": null,
    "lng": null
  }
}
```

### About Country
> Get detailed information about country (ISO3166-1)

* Action: `/api/geo/about/country/`
* Method: GET
* Required Params: `{query: "RU"}`
* Success Response:
```
{
  "success": true,
  "error": null,
  "result": {
    "country_data_or_code": "RU",
    "data": {
      "continent": "Europe",
      "address_format": "{{recipient}}\n{{postalcode}} {{city}}\n{{street}}\n{{country}}",
      "alpha2": "RU",
      "alpha3": "RUS",
      "country_code": "7",
      "international_prefix": "810",
      "ioc": "RUS",
      "gec": "RS",
      "name": "Russian Federation",
      "national_destination_code_lengths": [
        3
      ],
      "national_number_lengths": [
        10
      ],
      "national_prefix": "8",
      "number": "643",
      "region": "Europe",
      "subregion": "Eastern Europe",
      "world_region": "EMEA",
      "un_locode": "RU",
      "nationality": "Russian",
      "postal_code": true,
      "unofficial_names": [
        "Russia",
        "Russland",
        "Russie",
        "Rusia",
        "ロシア連邦",
        "Rusland",
        "Россия",
        "Расія"
      ],
      "languages_official": [
        "ru"
      ],
      "languages_spoken": [
        "ru"
      ],
      "geo": {
        "latitude": 61.52401,
        "latitude_dec": "63.125186920166016",
        "longitude": 105.318756,
        "longitude_dec": "103.75398254394531",
        "max_latitude": 82.1673907,
        "max_longitude": -168.9778799,
        "min_latitude": 41.185353,
        "min_longitude": 19.6160999,
        "bounds": {
          "northeast": {
            "lat": 82.1673907,
            "lng": -168.9778799
          },
          "southwest": {
            "lat": 41.185353,
            "lng": 19.6160999
          }
        }
      },
      "currency_code": "RUB",
      "translations": {
        "en": "Russian Federation"
      },
      "translated_names": [
        "Russian Federation"
      ]
    }
  }
}
```
* Error Response:
```
{
  "success": false,
  "error": "Not found",
  "result": null
}
```

### Distance
> Get distance from objects

* Action: `/api/geo/distance`
* Method: GET
* Required Params: `{from: {lat: 47.858205, lng: 2.294359}, to: {lat: 40.748433, lng: -73.985655}}`
* Optional Params: `{units: 'km'}`. Default: `mi`. Available: `km`, `mi`, `nm`
* Success Response:
```
{
  "success": true,
  "error": null,
  "result": {
    "distance": 3648.3340765758867,
    "units": "mi"
  }
}
```
* Error Response:
```
{
  "success": false,
  "error": "invalid params",
  "result": {
    "distance": null,
    "units": "mi"
  }
}
```